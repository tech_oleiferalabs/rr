package com.oleiferalabs.rr.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

public abstract class FirestoreUtil {
	
	private static Firestore db=null;
	
	public static Firestore getFirestore() throws Exception{
		//InputStream serviceAccount  = null;
		InputStream serviceAccount = FirestoreUtil.class.getClassLoader().getResourceAsStream("firestore-config.json");
		
		//File file = new File(FirestoreUtil.class.getClassLoader().getResource("firestore-config.json").getFile());
		//File is found
       // System.out.println("File Found : " + file.exists());
         
        //Read File Content
       // String content = new String(Files.readAllBytes(file.toPath()));
       // System.out.println(content);
		
		GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
		FirebaseOptions options = new FirebaseOptions.Builder()
		    .setCredentials(credentials)
		    .build();
		FirebaseApp.initializeApp(options);

		 db = FirestoreClient.getFirestore();
		return db;
	}
	public static Firestore getFirestoreConn() {
		try {
		if(db==null)
			return getFirestore();
		return db;
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
		
	}

}
