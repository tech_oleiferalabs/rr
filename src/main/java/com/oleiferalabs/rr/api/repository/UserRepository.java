package com.oleiferalabs.rr.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.oleiferalabs.rr.api.entity.UserInfo;

public interface UserRepository extends JpaRepository<UserInfo, Integer>{
	
	UserInfo findByEmail(String email);

}
