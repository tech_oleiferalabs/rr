package com.oleiferalabs.rr.api.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oleiferalabs.rr.api.model.ApiError;
import com.oleiferalabs.rr.api.model.ModelApiResponse;
import com.oleiferalabs.rr.api.model.Shop;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ShopController {
	@ApiOperation(value = "shop details", notes = "", response = ModelApiResponse.class, tags={ "SHOP", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Request user", response = ModelApiResponse.class),
        @ApiResponse(code = 400, message = "Request user", response = ApiError.class) })
    
    @RequestMapping(value = "/rr/api/shop",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
  public ResponseEntity<ModelApiResponse> createShopInfo(@ApiParam(value = "Request header" ) @RequestHeader(value="Authorization", required=false) String authorization,
	        @ApiParam(value = "Request for Shop"  )  @Valid @RequestBody Shop shop) {
	        // do some magic!
	        return new ResponseEntity<ModelApiResponse>(HttpStatus.OK);
  }
  @ApiOperation(value = "Shop details", notes = "", response = Shop.class, tags={ "SHOP", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Request user", response = Shop.class),
        @ApiResponse(code = 400, message = "Request user", response = ApiError.class) })
  @RequestMapping(value = "/rr/api/shop/{id}",
	        produces = { "application/json" }, 
	        method = RequestMethod.GET)
  public ResponseEntity<Shop> getShopInfo(@ApiParam(value = "",required=true ) @PathVariable("id") String id,
	        @ApiParam(value = "Request header" ) @RequestHeader(value="Authorization", required=false) String authorization) {
	        // do some magic!
	        return new ResponseEntity<Shop>(HttpStatus.OK);
  }
}
