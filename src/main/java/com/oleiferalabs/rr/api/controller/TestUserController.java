package com.oleiferalabs.rr.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.oleiferalabs.rr.api.util.FirestoreUtil;

@RestController
public class TestUserController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@PostMapping(path = "/rr/api/test")
	public @ResponseBody String create_user(@RequestHeader("Authorization") String authorization) {
		logger.info("getSuspectData ::: >>>> ");
		return getData();
		//return "Hello World !!!";
	}
	public String getData() {
		String email="";
		try {
		// asynchronously retrieve all users
		Firestore db=	FirestoreUtil.getFirestoreConn();
		System.out.println(db);
		ApiFuture<QuerySnapshot> query = db.collection("oleifera-contact-collection").get();
		// ...
		// query.get() blocks on response
		QuerySnapshot querySnapshot = query.get();
		List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
		for (QueryDocumentSnapshot document : documents) {
		  System.out.println("User: " + document.getId());
		  email = document.getString("email");
		  System.out.println("Email: " + email);
		  System.out.println("Name: " + document.getString("name"));
		}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return email;
	}
	
}


//java -jar swagger-codegen-cli-2.2.3.jar generate -i Oleifera-RR-API-1.0.0-resolved-2.json --api-package com.oleiferalabs.rr.api.controller --model-package com.oleiferalabs.rr.api.model -o target

//Oleifera-RR-API-1.0.0-resolved-2