package com.oleiferalabs.rr.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.oleiferalabs.rr.api.model.ApiError;
import com.oleiferalabs.rr.api.model.JwtResponse;
import com.oleiferalabs.rr.api.model.LoginRequest;
import com.oleiferalabs.rr.api.model.LoginResponse;
import com.oleiferalabs.rr.api.model.ModelApiResponse;
import com.oleiferalabs.rr.api.model.Owner;
import com.oleiferalabs.rr.api.model.Shop;
import com.oleiferalabs.rr.api.model.UserRequest;
import com.oleiferalabs.rr.api.service.JwtUserDetailsService;
import com.oleiferalabs.rr.api.service.UserService;
import com.oleiferalabs.rr.api.util.JwtTokenUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-11-17T15:30:23.291+05:30")

@Controller
public class UserController {
	Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@ApiOperation(value = "get user", notes = "", response = ModelApiResponse.class, tags={ "USER", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Request user", response = ModelApiResponse.class),
        @ApiResponse(code = 400, message = "Request user", response = ApiError.class) })
    
    @RequestMapping(value = "/rr/api/user",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
	public ResponseEntity<ModelApiResponse> createUser(@ApiParam(value = "Request user"  )  @Valid @RequestBody UserRequest user) {
		logger.info("<<<<<<<<<<<  inside createUser >>>>>>>>>>>>>");
		ModelApiResponse modelApiResponse = userService.saveUser(user);
		logger.info("ModelApiResponse >>>>> " +modelApiResponse);
        return new ResponseEntity<ModelApiResponse>(modelApiResponse,HttpStatus.OK);
    }

	@ApiOperation(value = "get user", notes = "", response = LoginResponse.class, tags={ "USER", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Request user", response = LoginResponse.class),
        @ApiResponse(code = 400, message = "Request user", response = ApiError.class) })
    
    @RequestMapping(value = "/rr/api/login",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
	public ResponseEntity<LoginResponse> getUser(@ApiParam(value = "Request user"  )  @Valid @RequestBody LoginRequest user) throws Exception{
		
		
		authenticate(user.getUsername(), user.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(user.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		LoginResponse res = new LoginResponse();
		res.setToken(token);

		return ResponseEntity.ok(res);
		
        //sreturn new ResponseEntity<LoginResponse>(HttpStatus.OK);
    }
	
	
	private void authenticate(String username, String password) throws Exception {
		try {
			logger.info("<<<<<<<<<<<<<< 2 >>>>>>>>>>>>>>>");
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

	  
	  
	
	

}
