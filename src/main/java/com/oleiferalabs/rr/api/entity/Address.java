package com.oleiferalabs.rr.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "address")
@Data
public class Address {
	  @Id  
	  @GeneratedValue(strategy=GenerationType.IDENTITY) 
	  private Long id;
	  private String address1;
	  private String address2;
	  private String city;
	  private String state;
	  private String pincode;
	  private String country;
	  @Column(name = "address_type")
	  private String addressType;
}
