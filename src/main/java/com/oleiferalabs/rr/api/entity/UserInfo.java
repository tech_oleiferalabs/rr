package com.oleiferalabs.rr.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Entity
@Table(name = "user_info")
@Data
public class UserInfo {
	
	  @Id  
	  @GeneratedValue(strategy=GenerationType.IDENTITY) 
	  private Long id;
	  @NotBlank
	  private String username;
	  @NotBlank
	  private String password;  
	  private String email;
	  @Column(name="user_id")
	  private String userId;
	  @Column(name="provider_name")
	  private String providerName;
	  private String token;
}
