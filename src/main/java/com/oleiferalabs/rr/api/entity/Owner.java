package com.oleiferalabs.rr.api.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "owner")
@Data
public class Owner {
	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Long id;
	private String firstname;
	private String lastname;
	@Column(name="owner_id")
	private String ownerId;
	private String userId;
	private String addressId;

}
