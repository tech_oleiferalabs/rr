package com.oleiferalabs.rr.api.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.oleiferalabs.rr.api.entity.UserInfo;
import com.oleiferalabs.rr.api.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("<<<<<<<<<<<<<< 5 >>>>>>>>>>>>>>>");
		UserInfo userInfo  =userRepository.findByEmail(username);
		//logger.info("userInfo >>>>>> "+userInfo);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		if (userInfo!=null) {
			return new User(username, encoder.encode(userInfo.getPassword()),
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
}
