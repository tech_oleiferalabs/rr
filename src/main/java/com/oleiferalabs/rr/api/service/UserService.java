package com.oleiferalabs.rr.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oleiferalabs.rr.api.entity.UserInfo;
import com.oleiferalabs.rr.api.model.ModelApiResponse;
import com.oleiferalabs.rr.api.model.UserRequest;
import com.oleiferalabs.rr.api.repository.UserRepository;

@Component
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public ModelApiResponse saveUser(UserRequest user) {
		ModelApiResponse modelApiResponse = new ModelApiResponse();
		UserInfo userInfoEntity = new UserInfo();
		userInfoEntity.setEmail(user.getEmail());
		userInfoEntity.setPassword(user.getPassword());
		userInfoEntity.setProviderName(user.getProviderName());
		userInfoEntity.setToken(user.getToken());
		userInfoEntity.setUsername(user.getUsername());
		userInfoEntity.setUserId(user.getUserId());
		UserInfo persistantUserInfoEntity =  userRepository.save(userInfoEntity);
		if(persistantUserInfoEntity!=null) {
			modelApiResponse.setStatus("success");
		}else {
			modelApiResponse.setStatus("fail");
		}
		return modelApiResponse;	
		
	}

}
